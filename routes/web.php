<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('Home');
});

Route::get('/cart', function () {
  return view('Cart');
})->name('cart');

Route::get('/wishlist', function () {
  return view('Wishlist');
})->name('wishlist');

Route::get('/checkout', function () {
  return view('Checkout');
})->name('checkout');


