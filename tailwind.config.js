import * as url from "url"

/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./resources/**/*.blade.php",
    "./resources/**/*.js",
    "./resources/**/*.vue",
  ],
  theme  : {
    extend: {
      colors         : {
        'slate': {
          50 : '#f8fafc',
          100: '#f1f5f9',
          200: '#e2e8f0',
          300: '#cbd5e1',
          400: '#94a3b8',
          500: '#64748b',
          600: '#475569',
          700: '#334155',
          800: '#1e293b',
          900: '#0f172a',
          950: '#020617',
        },
        'gray' : {
          50 : '#f9fafb',
          100: '#f3f4f6',
          200: '#e5e7eb',
          300: '#d1d5db',
          400: '#9ca3af',
          500: '#6b7280',
          600: '#4b5563',
          700: '#374151',
          800: '#1f2937',
          900: '#111827',
          950: '#030712',
        },
        'red'  : {
          50 : '#fef2f2',
          100: '#fee2e2',
          200: '#fecaca',
          300: '#fca5a5',
          400: '#f87171',
          500: '#ef4444',
          600: '#dc2626',
          700: '#b91c1c',
          800: '#991b1b',
          900: '#7f1d1d',
          950: '#450a0a',
        },
        'rose' : {
          50 : '#fff1f2',
          100: '#ffe4e6',
          200: '#fecdd3',
          300: '#fda4af',
          400: '#fb7185',
          500: '#f43f5e',
          600: '#e11d48',
          700: '#be123c',
          800: '#9f1239',
          900: '#881337',
          950: '#4c0519',
        }
      },
      spacing        : {
        px  : '1px',
        pxx : '8px',
        0   : '0',
        0.5 : '0.125rem',
        0.55: '0.2rem',
        1   : '0.25rem',
        1.5 : '0.375rem',
        2   : '0.5rem',
        2.5 : '0.625rem',
        3   : '0.75rem',
        3.5 : '0.875rem',
        4   : '1rem',
        5   : '1.25rem',
        6   : '1.5rem',
        7   : '1.75rem',
        8   : '2rem',
        9   : '2.25rem',
        10  : '2.5rem',
        11  : '2.75rem',
        12  : '3rem',
        13  : '15px',
        14  : '3.5rem',
        15  : '10px',
        16  : '4rem',
        20  : '5rem',
        22  : '5.5rem',
        24  : '6rem',
        25  : '5px',
        28  : '7rem',
        29  : '25px',
        30  : '30%',
        32  : '8rem',
        33  : '20px',
        34  : '8.5rem',
        35  : '40%',
        36  : '9rem',
        37  : '47%',
        40  : '10rem',
        41  : '80px',
        44  : '11rem',
        48  : '12rem',
        52  : '13rem',
        56  : '14rem',
        60  : '15rem',
        64  : '16rem',
        70  : '17rem',
        72  : '18rem',
        75  : '19rem',
        80  : '20rem',
        82  : '21rem',
        85  : '26rem',
        88  : '30rem',
        96  : '24rem',
        97  : '32rem',
        99  : '38.5rem',
        100 : '14%',
        101 : '66%',
        102 : '200px',
        103 : '34vh',
        104 : '600px',
        105 : '1700px',
        106 : '74%',
        107 : '90%',
        108 : '1500px',
        109 : '1000px',
        110 : '80%',
        111 : '93rem',
        112 : '102rem',
        113 : '750px',
        114 : '80rem',
        115 : '73px',
        116 : '400px',
        117 : '95px',
        118 : '40px',
        119 : '36rem',
        120 : '17.5rem',
        121 : '1.2rem',
        122 : '8.5rem',
        123 : '1390px',
        124 : '30px',
        125 : '20%',
        126 : '100px',
        127 : '50px',
        128 : '120px',
        129 : '210vh',
        130 : '250px',
        131 : '450px',
        132 : '1300px',
        133 : '70vh',
        134 : '5%',
        135 : '27%',
        136 : '14%',
        137 : '250px',
        138 : '66px',
        139 : '17px',
        140 : '300px',
        141 : '663px'

      },
      zIndex         : {
        '9'  : '9',
        '100': '99999'
      },
      boxShadow      : {
        'hidden': 'none'
      },
      fontSize       : {
        'xxs' : '14px',
        '1xl' : '30px',
        '10xl': '24px',
        '9xl' : '6rem',
        '11xl': '16px',
        '12xl': '2rem',
        '13xl': '20px',
        '14xl': '10px',
      },
      fontFamily     : {
        'poppins': ["Poppins", 'sans-serif'],
        'oswald' : ["oswald", 'sans-serif'],
        'lato'   : ["Lato", 'sans-serif'],
        'kanit'  : ["Kanit", 'sans-serif'],
        'bebas'  : ["Bebas Neue", 'sans-serif'],
        'robot'  : ["Roboto", 'sans-serif']
      },
      borderRadius   : {
        '100' : '100%'
      },
      borderWidth    : {
        '1' : '1px',
        '10': '1rem'
      },
      screens        : {
        'sm': {'max': '950px'}
      },
      backgroundImage: {
        // 'home' : url('')
        'home': "url('http://testlocal.com/assets/images/BG-home1.jpg')"
      },
    },
    /*plugins: [
      require('@tailwindcss/typography'),
      require('@tailwindcss/forms'),
      require('@tailwindcss/aspect-ratio'),
      require('@tailwindcss/container-queries'),
    ],*/
  }
}
