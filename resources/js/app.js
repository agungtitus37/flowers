import './bootstrap';

// Toggle Navbar Active
const navbarNav = document.querySelector(".navbar-nav")

// ketika di click menu nya mumcul
document.querySelector("#menu").onclick = () => {
  navbarNav.classList.toggle("active")
}

//klik di luar navbar
const menu = document.querySelector("#menu")
document.addEventListener("click", function (e) {
  if (!menu.contains(e.target) && !navbarNav.contains(e.target)) {
    navbarNav.classList.remove("active")
  }
})

// JS For Carousel
let slideIndex = 0
showSlides()

function showSlides() {
  let i = 0
  let slides = document.getElementsByClassName("mySlides")
  let dots = document.getElementsByClassName("dot")

  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none"
  }
  slideIndex++
  if (slideIndex > slides.length) {
    slideIndex = 1
  }
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "")
  }
  slides[slideIndex - 1].style.display = "block"
  dots[slideIndex - 1].className += " active"
  setTimeout(showSlides, 6000)
}

const search = document.getElementById("search")
search.addEventListener("click", function (e) {
  e.preventDefault()
  // console.log('test123')
  document.getElementById("searchForm").classList.add("show")
})

const sClose = document.getElementById("searchForm")
sClose.addEventListener("click", function (e) {
  e.stopPropagation()
  sClose.classList.remove("show")
})

document.querySelector("#searchForm form input").onclick = (e) => {
  e.stopPropagation()
}

const shopping = document.getElementById("shopping-cart")
shopping.addEventListener("click", function (e) {
  e.preventDefault()
  document.getElementById("menuCart").classList.add("showChart")
})

const buttonClose = document.getElementById("menuCart")
buttonClose.addEventListener("click", function (e) {
  e.preventDefault()
  buttonClose.classList.remove("showChart")
})

document.querySelector("#button").onclick = (e) => {
  e.stopPropagation()
}

