<a href="/wishlist" class="block py-3 px-4 no-underline hover:bg-slate-50 hover:w-full hover:relative hover:text-center  ">
  {{$slot}}
</a>
