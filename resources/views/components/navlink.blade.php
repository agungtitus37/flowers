<a
  class="text-slate-900 inline-block text-sm my-0 mx-4 font-semibold hover:text-rose-400 after:content-[''] after:block after:p-2 after:border-b-4 after:border-b-gray-900 after:scale-x-0 hover:after:scale-x-50 hover:after:py-0 after:transition-all sm:block sm:my-6 sm:mx-6 sm:py-2 sm:px-2 sm:text-start"
  href="/">
  {{$slot}}
</a>
