@extends('Master')
@section('title','Cart')

@section('content')
  <div class="cart w-full bg-red-100 py-44 pb-16" id="cart">
    <div class="container w-full px-4">
      <div class="row-inner w-123 mx-4 flex justify-center flex-wrap">
        <div class="text-center w-125">
          <H1 class="text-4xl">Cart</H1>
          <div class="text-center   w-full">
            <ul class="flex justify-center flex-row">
              <li class="inline-block relative text-base mr-124">Home</li>
              <li class="relative right-3.5 text-base">
                <iconify-icon icon="uis:angle-right-b"></iconify-icon>
              </li>
              <li>Cart</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="cart-menu w-full h-129 pt-12 mb-126">
    <div class="px-4 w-full">
      <div class="row-cart mx-4 flex flex-wrap justify-center">
        <div class="cart-inner w-full h-screen">
          <div class="shoping-cart-inner">
            <table class="w-full mb-4 align-top caption-bottom table-inner">
              <tbody class="align-middle border-0 border-solid border-inherit">
              <tr class="bg-transparent table w-full">
                <td class="align-middle my-33 mx-25">x</td>
                <td class="align-middle my-33 mx-25">
                  <a href="#">
                    <img src="/assets/images/holiday-2.jpg" class="h-32 mb-4" alt="">
                  </a>
                </td>
                <td class="align-middle my-33 mx-25">
                  <h4 class="text-base text-gray-950 clear-both font-semibold font-poppins">Holiday Flower 2</h4>
                </td>
                <td class="align-middle my-33 mx-25">$130.00</td>
                <td class="align-middle my-33 mx-25 flex justify-center mt-14">
                  <div class="h-127 cart-minus-plus leading-10 w-128 text-center bg-gray-50 border-gray-50 text-13xl font-normal">
                    <button type="button" id="minus" class="h-full w-30 cursor-pointer float-left font-normal border-r-2 bg-gray-50 border-gray-50 text-13xl">-</button>
                    <input type="text" id="numbers" value="2" name="qtynumber" class="cart-plus-minus-box shadow-none border-none h-full mb-0 p-0 text-center w-35 font-normal relative right-8">
                    <button type="button" id="plus" class="h-full w-30 cursor-pointer float-left font-normal border-r-2 bg-gray-50 border-gray-50 text-13xl relative left-8">+</button>
                  </div>
                </td>
                <td class="align-middle my-33 mx-25">$260.00</td>
              </tr>
              <tr class="bg-transparent table w-full">
                <td class="align-middle my-33 mx-25">x</td>
                <td class="align-middle my-33 mx-25">
                  <a href="#">
                    <img src="/assets/images/holiday-2.jpg" class="h-32 my-4" alt="">
                  </a>
                </td>
                <td class="align-middle my-33 mx-25">
                  <h4 class="text-base text-gray-950 clear-both font-semibold font-poppins">Holiday Flower 2</h4>
                </td>
                <td class="align-middle my-33 mx-25">$130.00</td>
                <td class="align-middle my-33 mx-25 flex justify-center mt-14">
                  <div class="h-127 cart-minus-plus leading-10 w-128 text-center bg-gray-50 border-gray-50 text-13xl font-normal">
                    <button type="button" id="minus" class="h-full w-30 cursor-pointer float-left font-normal border-r-2 bg-gray-50 border-gray-50 text-13xl">-</button>
                    <input type="text" id="numbers" value="2" name="qtynumber" class="cart-plus-minus-box shadow-none border-none h-full mb-0 p-0 text-center w-35 font-normal relative right-8">
                    <button type="button" id="plus  " class="h-full w-30 cursor-pointer float-left font-normal border-r-2 bg-gray-50 border-gray-50 text-13xl relative left-8">+</button>
                  </div>
                </td>
                <td class="align-middle my-33 mx-25">$260.00</td>
              </tr>
              <tr class="bg-transparent table w-full">
                <td class="align-middle my-33 mx-25">x</td>
                <td class="align-middle my-33 mx-25">
                  <a href="#">
                    <img src="/assets/images/holiday-2.jpg" class="h-32 my-4" alt="">
                  </a>
                </td>
                <td class="align-middle my-33 mx-25">
                  <h4 class="text-base text-gray-950 clear-both font-semibold font-poppins">Holiday Flower 2</h4>
                </td>
                <td class="align-middle my-33 mx-25">$130.00</td>
                <td class="align-middle my-33 mx-25 flex justify-center mt-14">
                  <div class="h-127 cart-minus-plus leading-10 w-128 text-center bg-gray-50 border-gray-50 text-13xl font-normal">
                    <button type="button" id="minus" class="h-full w-30 cursor-pointer float-left font-normal border-r-2 bg-gray-50 border-gray-50 text-13xl">-</button>
                    <input type="text" id="numbers" value="2" name="qtynumber" class="cart-plus-minus-box shadow-none border-none h-full mb-0 p-0 text-center w-35 font-normal relative right-8">
                    <button type="button" id="plus  " class="h-full w-30 cursor-pointer float-left font-normal border-r-2 bg-gray-50 border-gray-50 text-13xl relative left-8">+</button>
                  </div>
                </td>
                <td class="align-middle my-33 mx-25">$260.00</td>
              </tr>
              <tr class="bg-transparent table w-full">
                <td class="align-middle my-33 mx-25">x</td>
                <td class="align-middle my-33 mx-25">
                  <a href="#">
                    <img src="/assets/images/holiday-2.jpg" class="h-32 my-4" alt="">
                  </a>
                </td>
                <td class="align-middle my-33 mx-25">
                  <h4 class="text-base text-gray-950 clear-both font-semibold font-poppins">Holiday Flower 2</h4>
                </td>
                <td class="align-middle my-33 mx-25">$130.00</td>
                <td class="align-middle my-33 mx-25 flex justify-center mt-14">
                  <div class="h-127 cart-minus-plus leading-10 w-128 text-center bg-gray-50 border-gray-50 text-13xl font-normal">
                    <button type="button" id="minus" class="h-full w-30 cursor-pointer float-left font-normal border-r-2 bg-gray-50 border-gray-50 text-13xl">-</button>
                    <input type="text" id="numbers" value="2" name="qtynumber" class="cart-plus-minus-box shadow-none border-none h-full mb-0 p-0 text-center w-35 font-normal relative right-8">
                    <button type="button" id="plus  " class="h-full w-30 cursor-pointer float-left font-normal border-r-2 bg-gray-50 border-gray-50 text-13xl relative left-8">+</button>
                  </div>
                </td>
                <td class="align-middle my-33 mx-25">$260.00</td>
              </tr>
              <tr class="bg-transparent table w-full">
                <td class="align-middle my-33 mx-25">x</td>
                <td class="align-middle my-33 mx-25">
                  <a href="#">
                    <img src="/assets/images/holiday-2.jpg" class="h-32 my-4" alt="">
                  </a>
                </td>
                <td class="align-middle my-33 mx-25">
                  <h4 class="text-base text-gray-950 clear-both font-semibold font-poppins">Holiday Flower 2</h4>
                </td>
                <td class="align-middle my-33 mx-25">$130.00</td>
                <td class="align-middle my-33 mx-25 flex justify-center mt-14">
                  <div class="h-127 cart-minus-plus leading-10 w-128 text-center bg-gray-50 border-gray-50 text-13xl font-normal">
                    <button type="button" id="minus" class="h-full w-30 cursor-pointer float-left font-normal border-r-2 bg-gray-50 border-gray-50 text-13xl">-</button>
                    <input type="text" id="numbers" value="2" name="qtynumber" class="cart-plus-minus-box shadow-none border-none h-full mb-0 p-0 text-center w-35 font-normal relative right-8">
                    <button type="button" id="plus  " class="h-full w-30 cursor-pointer float-left font-normal border-r-2 bg-gray-50 border-gray-50 text-13xl relative left-8">+</button>
                  </div>
                </td>
                <td class="align-middle my-33 mx-25">$260.00</td>
              </tr>
              <tr class="bg-transparent table w-full">
                <td colspan="6" class="align-middle my-33 mx-25">
                  <div class="text-left">
                    <input type="text" name="coupon" placeholder="Coupon Cart" class=" input-coupon h-127 bg-gray-300 text-center max-w-130 my-4">
                    <button type="submit" class=" coupon-button h-127 w-130 bg-gray-600 text-gray-50 rounded-none inline-block font-medium text-center my-4 relative cursor-pointer left-4 hover:bg-gray-50 hover:text-gray-800">Submit Coupon</button>
                  </div>
                </td>
                <td class="align-middle my-33 mx-25">
                  <button type="submit" class=" button-update cursor-not-allowed pointer-events-none opacity-65 last:mr-0 h-127 w-130 bg-gray-600 text-gray-50 rounded-0 inline-block font-medium text-center relative hover:bg-gray-50 hover:text-gray-950 left-28"> Update Coupon</button>
                </td>
              </tr>
              </tbody>
            </table>
          </div>
          <div class="sub-total float-right mt-127 max-w-131 w-full">
            <h4 class="text-center font-bold text-2xl">Cart Totals</h4>
            <table class="table-inner align-top text-gray-600 w-full caption-bottom my-0">
              <tbody class="border-inherit border-0 border-solid">
              <tr class="bg-gray-50">
                <td>Cart Sub Total</td>
                <td>$689.00</td>
              </tr>
              <tr class="bg-gray-50">
                <td>Shipping And Handing</td>
                <td>$20.00</td>
              </tr>
              <tr class="bg-gray-50">
                <td>Vat</td>
                <td>$00.00</td>
              </tr>
              <tr class="bg-gray-50">
                <td>
                  <strong>Order Total</strong>
                </td>
                <td>
                  <strong>$709.00</strong>
                </td>
              </tr>
              </tbody>
            </table>
            <div class="mt-0 block mt-4">
              <a href="#" class=" btn block h-127 leading-10 bg-red-400 text-center text-gray-50 font-medium relative cursor-pointer hover:bg-gray-50 hover:text-gray-900">Processed To CheckOut</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

