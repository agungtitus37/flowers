@extends('Master')
@section('title', 'Checkout')
@section('content')
  <div class="cart w-full bg-red-100 py-24 pb-16" id="cart">
    <div class="container w-full px-4">
      <div class="row-inner w-123 mx-4 flex justify-center flex-wrap">
        <div class="text-center w-125">
          <H1 class="text-4xl font-bold font-oswald">Checkout</H1>
          <div class="text-center w-full">
            <ul class="flex justify-center flex-row">
              <li class="inline-block relative text-base mr-124">Home</li>
              <li class="relative right-3.5 text-base top-25">
                <iconify-icon icon="uis:angle-right-b"></iconify-icon>
              </li>
              <li>Checkout</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="mt-12">
    <div class="contaners p-4 m-8">
      <div class="row w-full flex justify-center text-center p-14">
        <div class="border-1 w-135 h-137 m-4">
          <div class="top-12 relative flex justify-center">
            <div class=" absolute border-2 h-138 w-138 rounded-100 bg-gray-900">
              <h1 class="text-gray-50 border-slate-800 absolute top-139 left-139">01</h1>
            </div>
            <div class="absolute top-16">
              <h2 class="text-gray-400 font-semibold">SHOPPING CARD</h2>
            </div>
            <div class="absolute bg-gray-950 content-[''] w-24 h-0.5 top-24"></div>
          </div>
        </div>
        <div class="border-1 w-135 h-137 m-4">
          <div class="top-12 relative flex justify-center">
            <div class=" absolute border-2 h-138 w-138 rounded-100 bg-gray-900">
              <h1 class="text-gray-50 border-slate-800 absolute top-139 left-139">02</h1>
            </div>
            <div class="absolute top-16">
              <h2 class="text-gray-400 font-semibold">CHECKOUT</h2>
            </div>
            <div class="absolute bg-gray-950 content-[''] w-24 h-0.5 top-24"></div>
          </div>
        </div>
        <div class="border-1 w-135 h-137 m-4">
          <div class="top-12 relative flex justify-center">
            <div class=" absolute border-2 h-138 w-138 rounded-100">
              <h1 class="text-gray-900 border-slate-800 absolute top-139 left-139">03</h1>
            </div>
            <div class="absolute top-16">
              <h2 class="text-gray-400 font-semibold">ORDER COMPLETED</h2>
            </div>
            <div class="absolute bg-gray-950 content-[''] w-24 h-0.5 top-24"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="mt-12 mb-12">
    <div class="mx-8">
      <div class="w-full flex justify-start">
        <div class="relative w-1/2">
          <h1 class="font-bold text-2xl">BILLING ADDRESS</h1>
          <div class=" mt-4">
            <h2 class="font-semibold font-robot">FULL NAME<span class="text-rose-400">*</span></h2>
            <input type="text" class="border-2 w-140 h-118 px-4 py-2 text-sm mt-1"
                   placeholder="Enter Your Full Name...">
          </div>
          <div class="absolute mt-4">
            <h2 class="font-semibold font-robot">EMAIL<span class="text-rose-400">*</span></h2>
            <input type="text" class="border-2 w-140 h-118 px-4 py-2 text-sm mt-1" placeholder="Enter Your Email...">
          </div>
          <div class="mt-4 ml-72">
            <h2 class="font-semibold font-robot">PHONE NUMBER<span class="text-rose-400">*</span></h2>
            <input type="text" class="border-2 w-140 h-118 px-4 py-2 text-sm mt-1"
                   placeholder="Enter Your Phone Number...">
          </div>
          <div class="mt-4">
            <h2 class="font-semibold font-robot">ADDRESS<span class="text-rose-400">*</span></h2>
            <input type="text" class="border-2 w-141 h-118 px-4 py-2 text-sm mt-1" placeholder="Enter Your Address...">
          </div>
        </div>
        <div class="relative w-1/2 left-8">
          <h1 class="font-bold text-xl font-robot">YOUR ORDER</h1>
          <div class="rows flex justify-between text-start mt-4">
            <div>
              <table class="mt-4 ">
                <tbody class="border-0 border-solid border-inherit float-left">
                <tr class="w-full border-b-gray-200">
                  <td class="text-gray-300 font-bold text-sm float-left w-137">PRODUCT NAME</td>
                  <td class="text-gray-300 font-bold text-sm w-137">QTY</td>
                  <td class="text-gray-300 font-bold text-sm float-right text-right">PRICE</td>
                </tr>
                <tr>
                  <td>
                    <h4 class="text-gray-400 font-bold text-sm py-4 w-137">Modern Chair</h4>
                    <h4 class="text-gray-400 font-bold text-sm py-4 w-137">Toldbod Lamp</h4>
                    <h4 class="text-gray-400 font-bold text-sm py-4 w-137">Getama Sofa</h4>
                  </td>
                  <td class="text-start">
                    <h4 class="py-4">01</h4>
                    <h4 class="py-4">02</h4>
                    <h4 class="py-4">03</h4>
                  </td>
                  <td>
                    <h4 class="py-4 text-right">$510.00</h4>
                    <h4 class="py-4 text-right">$210.00</h4>
                    <h4 class="py-4 text-right">$310.00</h4>
                  </td>
                </tr>
                <tr class="flex justify-between">
                  <td class="font-bold text-gray-400 text-sm py-2">CART SUBTOTAL</td>
                  <td class="font-bold text-gray-400 text-base py-2">$1030.00</td>
                </tr>
                <tr class="flex justify-between">
                  <td class="font-bold text-gray-400 text-sm py-2">SHIPPING & HANDLING</td>
                  <td class="font-bold text-gray-400 text-base py-2">FREE</td>
                </tr>
                <tr class="flex justify-between">
                  <td class="font-bold text-gray-400 text-sm py-2">ORDER TOTAL</td>
                  <td class="font-bold text-gray-400 text-base py-2">$1030.00</td>
                </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="w-1/2">
        <h1 class="text-2xl font-bold text-center">PAYMENT MENTHOD</h1>
        <div>
          <div class="mt-4">
            <input type="checkbox" id="transfer" name="transfer">
            <label for="transfer" class="font-bold text-xs">Direct Bank Transfer</label>
            <p class="text-14xl text-gray-500 relative left-4 w-8/12">Make your payment directly info our bank account.
              Please use your order ID as the payment reference. You product won't be shipped untill payment
              confiimation.</p>
          </div>
          <div class="mt-4">
            <input type="checkbox" id="transfer" name="transfer">
            <label for="transfer" class="font-bold text-xs">Cheque Payment</label>
          </div>
          <div class="mt-4">
            <input type="checkbox" id="transfer" name="transfer">
            <label for="transfer" class="font-bold text-xs">Paypal</label>
          </div>
          <div class="mt-4">
            <input type="checkbox" id="transfer" name="transfer">
            <label for="transfer" class="font-bold text-xs">I've raed and accept the</label>
            <div class="relative left-4">
              <a href="#" class="text-14xl text-red-500">Temr & conditions</a>
            </div>
          </div>
          <div class="mt-4">
            <div>
              <button type="submit"
                      class="border-2 py-25 px-1 w-full text-gray-50 bg-gray-950 hover:bg-gray-50 hover:text-gray-900 hover:border-gray-900 button-submid">
                PLACE ORDER
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
