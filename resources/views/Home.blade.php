@extends('Master')
@section('content')
  @include('Pages.MenuCart')

  @include('Pages.Slider')

  @include('Pages.Promotion')

  @include('Pages.Product')

  @include('Pages.Blog')
@endsection
