@extends('Master')
@section('title', 'WishList')
@section('content')
  <div class="cart w-full bg-red-100 py-24 pb-16" id="cart">
    <div class="container w-full px-4">
      <div class="row-inner w-123 mx-4 flex justify-center flex-wrap">
        <div class="text-center w-125">
          <H1 class="text-4xl font-bold font-oswald">WishList</H1>
          <div class="text-center   w-full">
            <ul class="flex justify-center flex-row">
              <li class="inline-block relative text-base mr-124">Home</li>
              <li class="relative right-3.5 text-base top-25">
                <iconify-icon icon="uis:angle-right-b"></iconify-icon>
              </li>
              <li>WishList</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="mb-126">
    <div class="containerss px-4 mx-8">
      <div class="row-wishlist flex flex-wrap mt-12">
        <div class="col px-4 relative w-full">
          <div>
            <div class="overflow-x-auto">
              <table class="w-full mb-4 align-top">
                <tbody class="border-b-2 border-gray-500 text-center">
                <tr class="bg-transparent table w-full border-inherit border-0 border-b-1 border-solid border-gray-300">
                  <td class="py-33 px-29">
                    <button type="button" class="hover:text-red-800">x</button>
                  </td>
                  <td class="py-33 px-29">
                    <a href="#">
                      <img src="/assets/images/holiday-2.jpg" alt="" class="max-w-126 text-center">
                    </a>
                  </td>
                  <td class="py-33 px-29">
                    <h4 class="font-bold">Flower Holiday 2</h4>
                  </td>
                  <td class="py-33 px-29">
                    <h4>$84.00</h4>
                  </td>
                  <td class="py-33 px-29">
                    <h4>In Stock</h4>
                  </td>
                  <td class="py-33 px-29">
                    <button type="button" class="bg-gray-600 text-gray-50 py-1.5 px-4 hover:bg-red-300 hover:transition-all">Add To Card</button>
                  </td>
                </tr>
                <tr class="bg-transparent table w-full border-inherit border-0 border-b-1 border-solid">
                  <td class="py-33 px-29">
                    <button type="button" class="hover:text-red-800">x</button>
                  </td>
                  <td class="py-33 px-29">
                    <a href="#">
                      <img src="/assets/images/holiday-2.jpg" alt="" class="max-w-126">
                    </a>
                  </td>
                  <td class="py-33 px-29">
                    <h4 class="font-bold">Flower Holiday 2 S</h4>
                  </td>
                  <td class="py-33 px-29">
                    <h4>$84.00</h4>
                  </td>
                  <td class="py-33 px-29">
                    <h4>In Stock</h4>
                  </td>
                  <td class="py-33 px-29">
                    <button type="button" class="bg-gray-600 text-gray-50 py-1.5 px-4 hover:bg-red-300 hover:transition-all">Add To Card</button>
                  </td>
                </tr>
                <tr class="bg-transparent table w-full border-inherit border-0 border-b-1 border-solid">
                  <td class="py-33 px-29">
                    <button type="button" class="hover:text-red-800">x</button>
                  </td>
                  <td class="py-33 px-29">
                    <a href="#">
                      <img src="/assets/images/holiday-2.jpg" alt="" class="max-w-126">
                    </a>
                  </td>
                  <td class="py-33 px-29">
                    <h4 class="font-bold">Flower Holiday</h4>
                  </td>
                  <td class="py-33 px-29">
                    <h4>$84.00</h4>
                  </td>
                  <td class="py-33 px-29">
                    <h4>In Stock</h4>
                  </td>
                  <td class="py-33 px-29">
                    <button type="button" class="bg-gray-600 text-gray-50 py-1.5 px-4 hover:bg-red-300 hover:transition-all">Add To Card</button>
                  </td>
                </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
