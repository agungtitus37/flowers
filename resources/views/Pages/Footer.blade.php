<footer class="bg-red-100 text-center p-0 pt-32 relative sm:top-114">
  <img src="/assets/images/logo.png" alt=""
       class="logofooter absolute top-12 sm:left-28 sm:bottom-0 sm:top-12 sm:right-0"/>
  <div>
    <p class="about py-0 px-72 sm:px-0 sm:m-auto sm:w-107">
      Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident,
      modi? Consequuntur rerum iusto voluptatem repellat quas
    </p>
  </div>
  <div class="containerflower justify-between w-full flex absolute bottom-121 sm:hidden">
    <img src="/assets/images/flowerpngs.png" alt="" class="flowerpng1 w-100"/>
    <img src="/assets/images/flowerpngs.png" alt="" class="flowerpng2 w-100"/>
  </div>
  <div class="socials py-4 px-0 text-2xl">
    <!-- <p>Social Media :</p> -->
    <a class="m-4" href="#"><i class="fa-brands fa-square-instagram"></i></a>
    <a class="m-4" href="#"><i class="fa-brands fa-square-facebook"></i></a>
    <a class="m-4" href="#"><i class="fa-brands fa-square-twitter"></i></a>
  </div>

  <div class="links">
    <a class="m-4 hover:text-rose-400" href="#home">Home</a>
    <a class="m-4 hover:text-rose-400" href="#collection">collection</a>
    <a class="m-4 hover:text-rose-400" href="#product">Product</a>
    <a class="m-4 hover:text-rose-400" href="#blog">Blog</a>
  </div>
  <div class="scroll-up absolute flex justify-center text-center">
    <a href="#home" class="flex relative justify-center text-center w-118 h-118 bg-gray-50 bottom-120 text-gray-950 rotate-45 left-119 sm:left-122">
      <i class="-rotate-45 relative justify-center text-center text-3xl"><iconify-icon icon="uis:angle-up"></iconify-icon></i>
    </a>
  </div>
  <div class="credit bg-red-300 p-1 relative top-15">
    <p class="bottom-0.5 text-sm text-gray-50">Created By <a href="">The Flower</a>. | &copy; 2024.</p>
  </div>
</footer>
