<section class="product w-full h-109 sm:h-109 mt-12 relative bottom-48 sm:mt-97" id="product">
  <h1 class="text-center font-poppins text-gray-900 text-4xl title-product sm:text-base sm:leading-8">Shop by
    collection</h1>
  <h3 class="text-center font-poppins text-gray-900 text-base body-product">- All Category of TheFlower -</h3>

  <div class="w-full h-screen absolute">
    <div class="row flex pt-8 sm:flex-col sm:items-center">
      <div class="product-discount w-1/2 h-full relative sm:w-full">
        <img src="/assets/images/collection.png" alt="" class="menu-img-discount w-full sm:h-104">
        <div class="product-discount-text absolute left-40 sm:left-20 top-72 text-center sm:top-40">
          <h3 class="text-4xl text-gray-800 font-poppins sm:text-2xl">Christmas Day</h3>
          <h1 class="text-4xl text-gray-800 font-bebas pb-1 sm:text-2xl">Sale 10%</h1>
          <a href="#" class="bg-gray-900 text-gray-50 p-2 rounded-2xl sm:text-base">Shop Now</a>
        </div>
      </div>
      <div class="product-card justify-center w-1/2 relative flex flex-wrap h-109 sm:top-8 sm:justify-center">
        <div class="flower-card">
          <img src="/assets/images/collec-1.jpg" alt="" class="img-product-card w-110 sm:relative sm:w-full">
          <div class="card-text">
            <h3 class="product-card-title ml-8 mt-4">- Rose Quas -</h3>
          </div>
          <div>
            <p class="price ml-14 sm:mb-4">IDR 30K</p>
          </div>
        </div>
        <div class="flower-card">
          <img src="/assets/images/collec-1.jpg" alt="" class="img-product-card w-110 sm:relative sm:w-full">
          <div class="card-text">
            <h3 class="product-card-title ml-8 mt-4">- Rose Quas -</h3>
          </div>
          <div>
            <p class="price ml-14 sm:mb-4">IDR 30K</p>
          </div>
        </div>
        <div class="flower-card">
          <img src="/assets/images/collec-1.jpg" alt="" class="img-product-card w-110 sm:relative sm:w-full">
          <div class="card-text">
            <h3 class="product-card-title ml-8 mt-4">- Rose Quas -</h3>
          </div>
          <div>
            <p class="price ml-14 sm:mb-4">IDR 30K</p>
          </div>
        </div>
        <div class="flower-card">
          <img src="/assets/images/collec-1.jpg" alt="" class="img-product-card w-110 sm:relative sm:w-full">
          <div class="card-text">
            <h3 class="product-card-title ml-8 mt-4">- Rose Quas -</h3>
          </div>
          <div>
            <p class="price ml-14 sm:mb-4">IDR 30K</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
