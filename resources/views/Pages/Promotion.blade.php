<div class="promotions w-full mt-4 h-screen relative top-24 right-px sm:relative sm:top-auto" id="collection">
  <div class="row w-full flex flex-nowrap ml-0 sm:flex-col sm:items-center">
    <div class="promo1 w-1/4 m-4 sm:w-11/12">
      <a href="#">
        <img src="/assets/images/banner1.jpg" class="promos1 w-full">
      </a>
    </div>
    <div class="promo1 w-1/4 m-4 sm:w-11/12">
      <a href="#">
        <img src="/assets/images/banner2.jpg" class="promos1 w-full">
      </a>
    </div>
    <div class="promo1 w-1/4 m-4 sm:w-11/12">
      <a href="#">
        <img src="/assets/images/banner3.jpg" class="promos1 w-full">
      </a>
    </div>
    <div class="promo1 w-1/4 m-4 sm:w-11/12">
      <a href="#">
        <img src="/assets/images/banner4.jpg" class="promos1 w-full">
      </a>
    </div>
  </div>
</div>
