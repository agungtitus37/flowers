<div class="carousel box-border w-full h-screen sm:h-103" id="home">
  <div class="slideshow-container relative m-auto">
    <div class="mySlides fade w-full h-102">
      <img src="/assets/images/slider-1.png" alt="" class="slide1 absolute object-cover w-full">
      <div class="text text-center top-0 relative pt-44 sm:pt-24 sm:w-full">
        <h3 class=" text-title text-5xl font-oswald sm:text-xs sm:relative sm:bottom-10 sm:m-auto sm:font-bold">EXPLORE
          THE NEW ARRIVALS</h3>
        <p
          class=" text-bodyCarousel relative w-4/12 left-96 text-base top-8 font-lato sm:w-37 sm:left-0 sm:text-xxs sm:leading-4 sm:font-bold sm:top-auto sm:bottom-8 sm:overflow-hidden sm:m-auto">
          It is a long established fact that a reader will be distracted by
          the readable content of a page when looking at its layout
        </p>
        <div class="pattern w-full h-16 text-center top-4 relative sm:absolute sm:top-22">
          <img src="/assets/images/Pattern.png" alt="" class="pattern-images absolute top-6 sm:w-1/2 sm:left-20"/>
        </div>
        <div class="relative top-4 left-24 w-10/12 sm:left-6 sm:top-0">
          <a href="#" class="shop bg-gray-950 rounded-2xl text-slate-50 p-2">Shop Now</a>
        </div>
      </div>
    </div>
    <div class="mySlides fade w-full h-102">
      <img src="/assets/images/slider-2.png" alt="" class="slide1 absolute object-cover w-full"/>
      <div class="carousel_text text-justify top-0 left-0 relative pt-28 h-16 w-96 pl-16 sm:absolute sm:top-8">
        <h3 class="perfect text-4xl font-oswald sm:absolute sm:font-bold sm:text-xs sm:left-12 sm:top-6">A Perfect</h3>
        <h1 class="bouquet text-5xl font-oswald font-bold sm:absolute sm:top-10 sm:text-base sm:left-12">Bouquet</h1>
        <div class="line absolute bg-gray-950 content-[''] w-24 h-3.5 mt-4 sm:h-0.55 sm:w-8 sm:top-12 sm:left-12"></div>
        <p
          class="font-lato text-base mt-12 sm:absolute sm:top-0 sm:left-0 sm:text-xxs leading-4 sm:whitespace-nowrap sm:overflow-hidden overflow-ellipsis sm:w-30 sm:top-8 sm:left-12">
          It is a long established fact that a reader will be distracted by
          the redable content of a page when looking at its latout
        </p>
      </div>
      <div class="relative top-60 left-16 w-10/12 text-justify sm:absolute sm:left-12 sm:top-34">
        <a href="#" class="shop2 bg-gray-950 rounded-2xl text-slate-50 p-2">Shop Now</a>
      </div>
    </div>
    <div class="mySlides fade w-full h-102">
      <img src="/assets/images/slider-1.png" alt="" class="slide1 absolute object-cover w-full">
      <div class="text text-center top-0 relative pt-44 sm:pt-24 sm:w-full">
        <h3 class=" text-title text-5xl font-oswald sm:text-xs sm:relative sm:bottom-10 sm:m-auto sm:font-bold">EXPLORE
          THE NEW ARRIVALS</h3>
        <p
          class=" text-bodyCarousel relative w-4/12 left-96 text-base top-8 font-lato sm:w-37 sm:left-0 sm:text-xxs sm:leading-4 sm:font-bold sm:top-auto sm:bottom-8 sm:overflow-hidden sm:m-auto">
          It is a long established fact that a reader will be distracted by
          the readable content of a page when looking at its layout
        </p>
        <div class="pattern w-full h-16 text-center top-4 relative sm:absolute sm:top-22">
          <img src="/assets/images/Pattern.png" alt="" class="pattern-images absolute top-6 sm:w-1/2 sm:left-20"/>
        </div>
        <div class="relative top-4 left-24 w-10/12 sm:left-6 sm:top-0">
          <a href="#" class="shop bg-gray-950 rounded-2xl text-slate-50 p-2">Shop Now</a>
        </div>
      </div>
    </div>
    <div class="mySlides fade w-full h-102">
      <img src="/assets/images/slider-2.png" alt="" class="slide1 absolute object-cover w-full"/>
      <div class="carousel_text text-justify top-0 left-0 relative pt-28 h-16 w-96 pl-16 sm:top-6">
        <h3 class="perfect text-4xl font-oswald sm:absolute sm:font-bold sm:text-xs sm:left-12 sm:top-6">A Perfect</h3>
        <h1 class="bouquet text-5xl font-oswald font-bold sm:absolute sm:top-10 sm:text-base sm:left-12">Bouquet</h1>
        <div class="line absolute bg-gray-950 content-[''] w-24 h-3.5 mt-4 sm:h-0.55 sm:w-8 sm:top-12 sm:left-12"></div>
        <p
          class="font-lato text-base mt-12 sm:absolute sm:top-0 sm:left-0 sm:text-xxs leading-4 sm:whitespace-nowrap sm:overflow-hidden overflow-ellipsis sm:w-30 sm:top-8 sm:left-12">
          It is a long established fact that a reader will be distracted by
          the redable content of a page when looking at its latout
        </p>
      </div>
      <div class="relative top-60 left-16 w-10/12 text-justify sm:absolute sm:left-12 sm:top-34">
        <a href="#" class="shop2 bg-gray-950 rounded-2xl text-slate-50 p-2">Shop Now</a>
      </div>
    </div>
    <div class="text-center relative top-32">
      <a
        class="prev cursor-pointer absolute top-1/2 w-auto -mt-8  p-4 text-slate-50 font-bold select-none text-xl hover:bg-gray-900 left-0"
        onclick="plusSlides(-1)">&#10094;</a>
      <a
        class="next cursor-pointer absolute top-1/2 w-auto -mt-8  p-4 text-slate-50 font-bold select-none text-xl hover:bg-gray-900 right-0"
        onclick="plusSlides(1)">&#10095;</a>
    </div>
  </div>
  <div class="bullets relative bottom-0 top-82 sm:hidden">
    <span class="dot cursor-pointer h-4 w-4 my-0 mx-0.5 bg-slate-50 inline-block" onclick="currentSlide(1)"></span>
    <span class="dot cursor-pointer h-4 w-4 my-0 mx-0.5 bg-slate-50 inline-block" onclick="currentSlide(2)"></span>
    <span class="dot cursor-pointer h-4 w-4 my-0 mx-0.5 bg-slate-50 inline-block" onclick="currentSlide(3)"></span>
    <span class="dot cursor-pointer h-4 w-4 my-0 mx-0.5 bg-slate-50 inline-block" onclick="currentSlide(4)"></span>
  </div>
</div>
