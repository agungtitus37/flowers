<div class="menu-cart fixed top-0 right-0 overflow-hidden w-116 h-screen block shadow-none bg-gray-50 top-117 sm:top-115"
     id="menuCart">
  <div class="menu-inner relative flex flex-col h-full pr-33 touch-auto scroll-bar">
    <div class="menu-head flex justify-center mb-29 pb-13 items-center">
      <span class="menu-title font-medium uppercase">Cart</span>
      <button class="close-button bg-transparent text-1xl py-0 px-13 cursor-pointer absolute right-0" id="closeBtn">X
      </button>
    </div>
    <div class="product-area scroll-bar ">
      <div class="mini-cart-product pt-33 pl-15 mb-33 clearfix">
        <div class="mini-cart-img w-41 mr-13 relative">
          <a href="#" class="mini-img no-underline">
            <img src="/assets/images/holiday-1.jpg" class="img bg-gray-50 max-w-full" alt="Flower Holiday">
          </a>
          <span class="item-delete absolute left-40 top-4 h-33 w-33 text-10xl text-center block cursor-pointer">
            <i class="fa-solid fa-trash"></i>
          </span>
        </div>
        <div class="cart-info overflow-hidden">
          <h6 class="mb-25 text-xxs text-slate-700 font-semibold">Holliday Flower</h6>
          <span class="quantity-flower text-xxs font-normal">1 X $65.00</span>
        </div>
      </div>
      <div class="mini-cart-product pt-33 pl-15 mb-33 clearfix">
        <div class="mini-cart-img w-41 mr-13 relative">
          <a href="#" class="mini-img no-underline">
            <img src="/assets/images/holiday-1.jpg" class="img bg-gray-50 max-w-full" alt="Flower Holiday">
          </a>
          <span class="item-delete absolute left-40 top-4 h-33 w-33 text-10xl text-center block cursor-pointer">
            <i class="fa-solid fa-trash"></i>
          </span>
        </div>
        <div class="cart-info overflow-hidden">
          <h6 class="mb-25 text-xxs text-slate-700 font-semibold">Holliday Flower</h6>
          <span class="quantity-flower text-xxs font-normal">1 X $65.00</span>
        </div>
      </div>
      <div class="mini-cart-product pt-33 pl-15 mb-33 clearfix">
        <div class="mini-cart-img w-41 mr-13 relative">
          <a href="#" class="mini-img no-underline">
            <img src="/assets/images/holiday-1.jpg" class="img bg-gray-50 max-w-full" alt="Flower Holiday">
          </a>
          <span class="item-delete absolute left-40 top-4 h-33 w-33 text-10xl text-center block cursor-pointer">
            <i class="fa-solid fa-trash"></i>
          </span>
        </div>
        <div class="cart-info overflow-hidden">
          <h6 class="mb-25 text-xxs text-slate-700 font-semibold">Holliday Flower</h6>
          <span class="quantity-flower text-xxs font-normal">1 X $65.00</span>
        </div>
      </div>
      <div class="mini-cart-product pt-33 pl-15 mb-33 clearfix">
        <div class="mini-cart-img w-41 mr-13 relative">
          <a href="#" class="mini-img no-underline">
            <img src="/assets/images/holiday-1.jpg" class="img bg-gray-50 max-w-full" alt="Flower Holiday">
          </a>
          <span class="item-delete absolute left-40 top-4 h-33 w-33 text-10xl text-center block cursor-pointer">
            <i class="fa-solid fa-trash"></i>
          </span>
        </div>
        <div class="cart-info overflow-hidden">
          <h6 class="mb-25 text-xxs text-slate-700 font-semibold">Holliday Flower</h6>
          <span class="quantity-flower text-xxs font-normal">1 X $65.00</span>
        </div>
      </div>
    </div>
    <div class="cart-footer mt-29">
      <div class="cart-sub-total">
        <h5 class="flex justify-between text-center mb-0 text-11xl font-semibold mx-8">
          SubTotal :
          <span class="text-rose-600 font-semibold">$310.00</span>
        </h5>
      </div>
      <div class="button-warp mt-0 flex justify-between" id="button">
        <a href="/cart"
           class="theme-btn-1 btn btn-effect-1 border-2 border-rose-400 inline-block text-center cursor-pointer relative bg-rose-400 text-gray-50 hover:bg-gray-50 hover:text-gray-800">View
          Cart</a>
        <a href="/checkout"
           class="theme-btn-2 btn btn-effect-2 border-2 border-gray-700 bg-gray-700 text-gray-50 hover:bg-gray-50 hover:text-gray-800">Check
          Out</a>
      </div>
      <p class="free font-light mt-4 text-center hyphens-auto text-xxs">Free Shipping in All Orders Over $100</p>
    </div>
  </div>
</div>
