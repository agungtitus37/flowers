<section class="blog w-full top-0 left-0 h-screen mt-16 relative sm:h-113 sm:top-99" id="blog">
  <h1 class="blog-title text-center text-5xl text-gray-800 font-poppins text-5xl">LATES POST</h1>
  <h3 class="blog-body text-center mt-4 font-poppins text-base sm:m-auto sm:w-106">Lorem ipsum dolor sit amet,
    consectetur adipisicing
    elit. Illum, rem</h3>
  <div class="blog-card w-full h-full flex justify-center p-14 sm:flex-col sm:block sm:p-8">
    <div class="card sm:w-full">
      <img src="/assets/images/blog-1.jpg" alt="" class="w-11/12 sm:w-full">
      <div class="calendar text-center sm:w-full sm:left-0 sm:right-0 sm:relative">
            <span class="apr text-xs border-r-2 border-r-gray-500">
              <i class="fa-solid fa-calendar-days calendars"></i
              ><span class="cals px-2">28 April 2023</span>
            </span>
        <span class="time pl-2 text-sm">
              <i class="fa-solid fa-pen-to-square calendars"></i
              ><span class="cals pl-2">lorem ipsum</span>
            </span>
      </div>
      <div class="text-body">
        <p class="text-xs text-justify pt-4 sm:pb-8">
          Lorem, ipsum dolor sit amet consectetur adipisicing elit. Expedita
          facere ipsam cupiditate inventore perspiciatis repudiandae dicta,
          provident temporibus. Sequi, adipisci.
        </p>
      </div>
    </div>
    <div class="card sm:w-full">
      <img src="/assets/images/blog-1.jpg" alt="" class="w-11/12 sm:w-full">
      <div class="calendar text-center sm:w-full sm:left-0 sm:right-0 sm:relative">
            <span class="apr text-xs border-r-2 border-r-gray-500">
              <i class="fa-solid fa-calendar-days calendars"></i
              ><span class="cals px-2">28 April 2023</span>
            </span>
        <span class="time pl-2 text-sm">
              <i class="fa-solid fa-pen-to-square calendars"></i
              ><span class="cals pl-2">lorem ipsum</span>
            </span>
      </div>
      <div class="text-body">
        <p class="text-xs text-justify pt-4 sm:pb-8 ">
          Lorem, ipsum dolor sit amet consectetur adipisicing elit. Expedita
          facere ipsam cupiditate inventore perspiciatis repudiandae dicta,
          provident temporibus. Sequi, adipisci.
        </p>
      </div>
    </div>
    <div class="card sm:w-full">
      <img src="/assets/images/blog-1.jpg" alt="" class="w-11/12 sm:w-full">
      <div class="calendar text-center sm:w-full sm:left-0 sm:right-0 sm:relative">
            <span class="apr text-xs border-r-2 border-r-gray-500">
              <i class="fa-solid fa-calendar-days calendars"></i
              ><span class="cals px-2">28 April 2023</span>
            </span>
        <span class="time pl-2 text-sm">
              <i class="fa-solid fa-pen-to-square calendars"></i
              ><span class="cals pl-2">lorem ipsum</span>
            </span>
      </div>
      <div class="text-body">
        <p class="text-xs text-justify pt-4 sm:pb-8">
          Lorem, ipsum dolor sit amet consectetur adipisicing elit. Expedita
          facere ipsam cupiditate inventore perspiciatis repudiandae dicta,
          provident temporibus. Sequi, adipisci.
        </p>
      </div>
    </div>
  </div>
</section>
