<header id="header">
  <nav class="fixed flex justify-between center inset-x-0 top-0 p-2 text-center border-y-2 sm:pb-0 navbar">
    <div
      class="navbar-nav mt-4 sm:absolute sm:top-full sm:-right-96 sm:bg-gray-600 sm:w-36 sm:text-slate-800 sm:mt-0 sm:h-96">
      <x-navlink>Home</x-navlink>
      <x-navlink>Collection</x-navlink>
      <x-navlink>Produce</x-navlink>
      <x-navlink>Blog</x-navlink>
    </div>
    <div class="nav-logo sm:w-1/2">
      <a href="" class="inline-block text-black mr-8 logo-images">
        <img src="/assets/images/logo.png" alt="" class="logo lg:w-3/4 w-101">
      </a>
    </div>
    <div class="navbar-icon items-baseline text-end justify-end w-1/4  flex right-12 sm:w-full relative sm:bottom-pxx sm:left-4">
      <div class="dropdown relative inline-block group">
        <a href="#" id="user" class="navbar-user inline-block mr-8 text-2xl"
        >
          <iconify-icon icon="tdesign:user-add"></iconify-icon>
        </a>
        <div id="menu-dropdown"
             class="dropdown-content hidden group-hover:block  absolute bg-gray-100 max-w-40 -ml-12 shadow">
          <x-user>Register</x-user>
          <x-user>Sign In</x-user>
          <x-user>Whitelist</x-user>
        </div>
      </div>
      <div>
        <a href="#" id="shopping-cart" class="inline-block mr-8 mt-4 text-2xl"
        >
          <iconify-icon icon="flowbite:cart-outline"></iconify-icon>
        </a>
      </div>
      <div class="navbar-search flex sm:w-full">
        <a href="#" id="search" class="inline-block relative sm:left-0 sm:w-2/6 text-2xl">
          <iconify-icon icon="ic:baseline-search"></iconify-icon>
        </a>
        <a href="#" id="menu" class="hidden sm:block sm:w-2/6 sm:ml-4 sm:text-xl"><i
            class="fa-solid fa-bars"></i></a>
      </div>
    </div>
    <div
      class="search-form fixed inset-y-0 bottom-0 top-0 w-full flex justify-center items-center flex-wrap -translate-y-full transition-all duration-75"
      id="searchForm">
      <div class="search-inner w-6/12 relative text-end">
        <form method="get" action="#">
          <button class="search-close py-0 px-4 top-0 relative hidden" id="closeForm">X</button>
          <div class="search-bar-inner relative h-14 ">
            <input
              type="text"
              name="s"
              id="s"
              placeholder="Search Here"
              class="search-bar-input w-full h-14"
            />
            <button class="search-button absolute inset-y-1 right-1 border-none bg-transparent w-14 cursor-pointer"
                    value="Search">
              <i class="fa-solid fa-magnifying-glass"></i>
            </button>
          </div>
        </form>
      </div>
    </div>
  </nav>
</header>
